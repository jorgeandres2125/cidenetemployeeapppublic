USE [master]
GO
/****** Object:  Database [CidenetEmployeeDB]    Script Date: 1/02/2021 7:50:21 a. m. ******/
CREATE DATABASE [CidenetEmployeeDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CidenetEmployeeDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\CidenetEmployeeDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CidenetEmployeeDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\CidenetEmployeeDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [CidenetEmployeeDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CidenetEmployeeDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CidenetEmployeeDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CidenetEmployeeDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CidenetEmployeeDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CidenetEmployeeDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CidenetEmployeeDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET RECOVERY FULL 
GO
ALTER DATABASE [CidenetEmployeeDB] SET  MULTI_USER 
GO
ALTER DATABASE [CidenetEmployeeDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CidenetEmployeeDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CidenetEmployeeDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CidenetEmployeeDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CidenetEmployeeDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CidenetEmployeeDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'CidenetEmployeeDB', N'ON'
GO
ALTER DATABASE [CidenetEmployeeDB] SET QUERY_STORE = OFF
GO
USE [CidenetEmployeeDB]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 1/02/2021 7:50:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[EmailDomain] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 1/02/2021 7:50:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](20) NOT NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](300) NOT NULL,
	[DateOfJoining] [datetime] NULL,
	[CountryId] [int] NOT NULL,
	[IdentificationId] [int] NOT NULL,
	[SecondSurename] [nvarchar](50) NULL,
	[WorkAreaId] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Identification]    Script Date: 1/02/2021 7:50:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Identification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdentificationTypeId] [int] NOT NULL,
	[Value] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Identification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdentificationType]    Script Date: 1/02/2021 7:50:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdentificationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_IdentificationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkArea]    Script Date: 1/02/2021 7:50:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkArea](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_WorkArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([Id], [Code], [Name], [EmailDomain]) VALUES (1, N'COL', N'COLOMBIA', N'cidenet.com.co')
INSERT [dbo].[Country] ([Id], [Code], [Name], [EmailDomain]) VALUES (2, N'EEUU', N'ESTADOS UNIDOS', N'cidenet.com.us')
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [FirstName], [MiddleName], [LastName], [Email], [DateOfJoining], [CountryId], [IdentificationId], [SecondSurename], [WorkAreaId]) VALUES (1, N'Juan', N'Carlos', N'Perez', N'juan.perez@cidenet.com.co ', CAST(N'2001-08-12T00:00:00.000' AS DateTime), 1, 1, N'Montoya', 3)
INSERT [dbo].[Employee] ([Id], [FirstName], [MiddleName], [LastName], [Email], [DateOfJoining], [CountryId], [IdentificationId], [SecondSurename], [WorkAreaId]) VALUES (3, N'Juan', N'Felipe', N'Perez', N'juan.perez.1@cidenet.com.co ', CAST(N'2001-08-12T00:00:00.000' AS DateTime), 1, 2, N'Gomez', 10)
INSERT [dbo].[Employee] ([Id], [FirstName], [MiddleName], [LastName], [Email], [DateOfJoining], [CountryId], [IdentificationId], [SecondSurename], [WorkAreaId]) VALUES (4, N'Juan', N'David', N'Perez', N'juan.perez.1@cidenet.com.co ', CAST(N'2001-08-12T00:00:00.000' AS DateTime), 1, 4, N'Jimenez', 7)
INSERT [dbo].[Employee] ([Id], [FirstName], [MiddleName], [LastName], [Email], [DateOfJoining], [CountryId], [IdentificationId], [SecondSurename], [WorkAreaId]) VALUES (5, N'Juan', N'Andres', N'Perez', N'juan.perez.5@cidenet.com.co

', CAST(N'2001-09-11T00:00:00.000' AS DateTime), 1, 5, N'Calle', 13)
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
SET IDENTITY_INSERT [dbo].[Identification] ON 

INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (1, 1, N'76984123')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (2, 2, N'123456789')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (3, 3, N'78934525')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (4, 4, N'6546574')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (5, 1, N'87687867')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (6, 1, N'7654326')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (7, 5, N'A243543453')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (8, 6, N'6545645677')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (9, 7, N'4435434534')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (10, 8, N'5645645680')
INSERT [dbo].[Identification] ([Id], [IdentificationTypeId], [Value]) VALUES (11, 9, N'4435453453')
SET IDENTITY_INSERT [dbo].[Identification] OFF
GO
SET IDENTITY_INSERT [dbo].[IdentificationType] ON 

INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (1, N'Cedula Ciudadania', 1)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (2, N'Cedula Estranjeria', 1)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (3, N'Pasaporte', 1)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (4, N'Permiso Especial', 1)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (5, N'Pasaporte', 2)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (6, N'Tarjeta Verde', 2)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (7, N'Licencia de Conducir', 2)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (8, N'Real ID', 2)
INSERT [dbo].[IdentificationType] ([Id], [Name], [CountryId]) VALUES (9, N'DNI', 2)
SET IDENTITY_INSERT [dbo].[IdentificationType] OFF
GO
SET IDENTITY_INSERT [dbo].[WorkArea] ON 

INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (1, N'Adminitración', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (2, N'Administración', 2)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (3, N'Financiera', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (4, N'Financiera', 2)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (5, N'Compras', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (6, N'Adquisiciones', 2)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (7, N'Infraestructura', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (9, N'Concierge', 2)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (10, N'Operacion', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (11, N'Operations', 2)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (13, N'Talento Humano', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (14, N'Human Resources', 2)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (15, N'Servicios Varios', 1)
INSERT [dbo].[WorkArea] ([Id], [Name], [CountryId]) VALUES (16, N'Hardware and Networking ', 2)
SET IDENTITY_INSERT [dbo].[WorkArea] OFF
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Country]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Identification] FOREIGN KEY([IdentificationId])
REFERENCES [dbo].[Identification] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Identification]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_WorkArea] FOREIGN KEY([WorkAreaId])
REFERENCES [dbo].[WorkArea] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_WorkArea]
GO
ALTER TABLE [dbo].[Identification]  WITH CHECK ADD  CONSTRAINT [FK_Identification_IdentificationType] FOREIGN KEY([IdentificationTypeId])
REFERENCES [dbo].[IdentificationType] ([Id])
GO
ALTER TABLE [dbo].[Identification] CHECK CONSTRAINT [FK_Identification_IdentificationType]
GO
ALTER TABLE [dbo].[IdentificationType]  WITH CHECK ADD  CONSTRAINT [FK_IdentificationType_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[IdentificationType] CHECK CONSTRAINT [FK_IdentificationType_Country]
GO
ALTER TABLE [dbo].[WorkArea]  WITH CHECK ADD  CONSTRAINT [FK_WorkArea_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[WorkArea] CHECK CONSTRAINT [FK_WorkArea_Country]
GO
USE [master]
GO
ALTER DATABASE [CidenetEmployeeDB] SET  READ_WRITE 
GO
