import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-list-of-employees',
  templateUrl: './list-of-employees.component.html',
  styleUrls: ['./list-of-employees.component.css']
})
export class ListOfEmployeesComponent implements OnInit {

  constructor(private service:EmployeeService) { }

  EmployeeList:any[];

  ngOnInit(): void {
    this.refreshEmployeeList();
  }


  refreshEmployeeList(){
    this.service.getListOfEmployees().subscribe(data=>{
      this.EmployeeList = data;
    });
  }
}
