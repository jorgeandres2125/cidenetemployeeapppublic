import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Components
import { ListOfEmployeesComponent } from './list-of-employees/list-of-employees.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';

const routes: Routes = [
  { path: '',   redirectTo: 'list-of-employees', pathMatch: 'full' },
  { path:'list-of-employees',component:ListOfEmployeesComponent },
  { path:'add-employee',component:AddEmployeeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
