package co.com.cidenet.rest.Country;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.domain.entity.Country;
import co.com.cidenet.error.Country.CountryNotFoundException;
import co.com.cidenet.service.CountryService;

@RestController
@RequestMapping("/api")
public class CountryRestController {

	@Autowired
	private CountryService countryService;
	
	@GetMapping("/countries")
	public List<Country> getCountries() {
		
		List<Country> list = countryService.getCountries(); 
		return list;
		
	}
	
	@GetMapping("/countries/{countryId}")
	public Country getCountry(@PathVariable int countryId) {
		
		Country theCountry = countryService.getCountry(countryId);
		
		if (theCountry == null) {
			throw new CountryNotFoundException("Country id not found - " + countryId);
		}
		
		return theCountry;
	}
	
}
