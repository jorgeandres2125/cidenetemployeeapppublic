package co.com.cidenet.rest.WorkArea;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.domain.entity.Country;
import co.com.cidenet.domain.entity.IdentificationType;
import co.com.cidenet.presentation.error.EntityNotFoundException;
import co.com.cidenet.presentation.dto.CountryDto;
import co.com.cidenet.presentation.dto.IdentificationTypeDto;
import co.com.cidenet.service.IdentificationTypeService;

@RestController
@RequestMapping("/api")
public class IdentificationTypeRestController {

	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private IdentificationTypeService identificationTypeService;
	
	
	@GetMapping("/identification-types/{countryCode}")
	public List<IdentificationTypeDto> getIdentificationTypesByCountry(@PathVariable String countryCode) {
		
		List<IdentificationType> list = identificationTypeService.getIdentificationTypesByCountry(countryCode); 
		if(list == null || list.isEmpty()) {
			throw new EntityNotFoundException("No encontro tipos de identificacion para el pais: "+countryCode);
		}
		return list.stream()
		          .map(this::convertToDto)
		          .collect(Collectors.toList());
	}
	
	@GetMapping("/identification-type/{identificationTypeId}")
	public IdentificationTypeDto getIdentificationType(@PathVariable int identificationTypeId) {
		
		IdentificationType theIdentificationType = identificationTypeService.getIdentificationType(identificationTypeId);
		
		if (theIdentificationType == null) {
			throw new EntityNotFoundException("Id " +identificationTypeId+" no encontrado");
		}
		
		return convertToDto(theIdentificationType);
	}
	
	private IdentificationTypeDto convertToDto(IdentificationType model) {
		IdentificationTypeDto dto = modelMapper.map(model, IdentificationTypeDto.class);
		dto.setCountryId(model.getCountry().getId());
		dto.setCountryCode(model.getCountry().getCode());
	    return dto;
	}
}
