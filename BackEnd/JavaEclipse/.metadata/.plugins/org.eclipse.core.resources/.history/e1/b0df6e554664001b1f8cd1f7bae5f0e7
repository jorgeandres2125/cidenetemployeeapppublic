package co.com.cidenet.rest.Country;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.domain.dto.CountryDto;
import co.com.cidenet.domain.entity.Country;
import co.com.cidenet.error.Country.CountryNotFoundException;
import co.com.cidenet.service.CountryService;

@RestController
@RequestMapping("/api")
public class CountryRestController {

	@Autowired
	private CountryService countryService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@GetMapping("/countries")
	public List<CountryDto> getCountries() {
		
		List<Country> list = countryService.getCountries(); 
		if(list == null || list.isEmpty()) {
			throw new CountryNotFoundException("No se encontraron paises");
		}
		return list.stream()
		          .map(this::convertToDto)
		          .collect(Collectors.toList());
	}
	
	@GetMapping("/countries/{countryId}")
	public Country getCountry(@PathVariable int countryId) {
		
		Country theCountry = countryService.getCountry(countryId);
		
		if (theCountry == null) {
			throw new CountryNotFoundException("Id " + countryId+" no encontrado");
		}
		
		return theCountry;
	}
	
	private CountryDto convertToDto(Country model) {
		CountryDto dto = modelMapper.map(model, CountryDto.class);
	    return dto;
	}
	
}
