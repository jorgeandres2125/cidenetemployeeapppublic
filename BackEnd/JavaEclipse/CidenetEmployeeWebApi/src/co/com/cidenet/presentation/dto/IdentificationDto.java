package co.com.cidenet.presentation.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import co.com.cidenet.domain.entity.IdentificationType;

public class IdentificationDto {
	private int id;
	private String value;
	private int identificationTypeId;
	private String identificationType;
	
	
	
	public IdentificationDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public IdentificationDto(int id, String value, int identificationTypeId, String identificationType) {
		super();
		this.id = id;
		this.value = value;
		this.identificationTypeId = identificationTypeId;
		this.identificationType = identificationType;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getIdentificationTypeId() {
		return identificationTypeId;
	}
	public void setIdentificationTypeId(int identificationTypeId) {
		this.identificationTypeId = identificationTypeId;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}
	
	
	
	
}
