package co.com.cidenet.presentation.dto;

import com.sun.istack.NotNull;

public class CountryDto {
	
    @NotNull
	private int id;
	
    @NotNull
	private String code;
	
    @NotNull
	private String name;
	
    @NotNull
	private String emailDomain;

	
	public CountryDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public CountryDto(int id, String code, String name, String emailDomain) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.emailDomain = emailDomain;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}
	
	
}
