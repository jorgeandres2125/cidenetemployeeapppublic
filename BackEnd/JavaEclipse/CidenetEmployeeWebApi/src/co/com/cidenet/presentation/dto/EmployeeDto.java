package co.com.cidenet.presentation.dto;

public class EmployeeDto {

	
	private int id;
	
	private String firstName;
	
	private String middleName;
	
	private String lastName;
	
	private String secondSurename;
	
	private String email;
	
	private String dateOfJoining;
	
	private int countryId;
	
	private String countryCode;
	
	private int workAreaId;
	
	private IdentificationDto identification;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSecondSurename() {
		return secondSurename;
	}

	public void setSecondSurename(String secondSurename) {
		this.secondSurename = secondSurename;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public int getWorkAreaId() {
		return workAreaId;
	}

	public void setWorkAreaId(int workAreaId) {
		this.workAreaId = workAreaId;
	}

	public IdentificationDto getIdentification() {
		return identification;
	}

	public void setIdentification(IdentificationDto identification) {
		this.identification = identification;
	}
	
	
	
	
}
