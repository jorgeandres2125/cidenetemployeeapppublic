package co.com.cidenet.presentation.dto;

public class IdentificationTypeDto {
	
	private int id;
	
	private String name;
	
	private int countryId;
	
	private String countryCode;
	
	public IdentificationTypeDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IdentificationTypeDto(int id, String name, int countryId, String countryCode) {
		super();
		this.id = id;
		this.name = name;
		this.countryId = countryId;
		this.countryCode = countryCode;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}


	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
	
	
}
