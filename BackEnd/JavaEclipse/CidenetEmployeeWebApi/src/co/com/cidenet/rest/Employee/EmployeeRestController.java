package co.com.cidenet.rest.Employee;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.domain.entity.Country;
import co.com.cidenet.domain.entity.Employee;
import co.com.cidenet.presentation.dto.CountryDto;
import co.com.cidenet.presentation.dto.EmployeeDto;
import co.com.cidenet.presentation.dto.IdentificationDto;
import co.com.cidenet.presentation.error.EntityNotFoundException;
import co.com.cidenet.service.EmployeeService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT, RequestMethod.DELETE})
public class EmployeeRestController {

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@GetMapping("/employees")
	public List<EmployeeDto> getCountries() {
		
		List<Employee> list = employeeService.getEmployees(); 
		if(list == null || list.isEmpty()) {
			throw new EntityNotFoundException("No se encontraron empleados");
		}
		return list.stream()
		          .map(this::convertToDto)
		          .collect(Collectors.toList());
	}
	
	
	private EmployeeDto convertToDto(Employee model) {
		EmployeeDto dto = modelMapper.map(model, EmployeeDto.class);
		dto.setCountryId(model.getCountry().getId());
		dto.setCountryCode(model.getCountry().getCode());
		dto.setDateOfJoining(DateToString(model.getDateOfJoining()));
		dto.setWorkAreaId(model.getWorkArea().getId());
		dto.setIdentification(new IdentificationDto(
				model.getIdentification().getId(), 
				model.getIdentification().getValue(), 
				model.getIdentification().getIdentificationType().getId(),
				model.getIdentification().getIdentificationType().getName()
				));
	    return dto;
	}
	
	private String DateToString(Date myDate) {
		if(myDate == null) return null;
		else {
			DateTime dateTime = new DateTime(myDate);
			return dateTime.toString("yyyy-MM-dd");
		}
	}
	
	
	
}
