package co.com.cidenet.rest.WorkArea;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.domain.entity.Country;
import co.com.cidenet.domain.entity.IdentificationType;
import co.com.cidenet.domain.entity.WorkArea;
import co.com.cidenet.presentation.error.EntityNotFoundException;
import co.com.cidenet.presentation.dto.CountryDto;
import co.com.cidenet.presentation.dto.IdentificationTypeDto;
import co.com.cidenet.presentation.dto.WorkAreaDto;
import co.com.cidenet.service.IdentificationTypeService;
import co.com.cidenet.service.WorkAreaService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT, RequestMethod.DELETE})
public class WorkAreaRestController {

	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private WorkAreaService workAreaService;
	
	
	@GetMapping("/work-areas/{countryCode}")
	public List<WorkAreaDto> getWorkAreasByCountry(@PathVariable String countryCode) {
		
		List<WorkArea> list = workAreaService.getWorkAreasByCountry(countryCode); 
		if(list == null || list.isEmpty()) {
			throw new EntityNotFoundException("No encontro areas de trabajo para el pais: "+countryCode);
		}
		return list.stream()
		          .map(this::convertToDto)
		          .collect(Collectors.toList());
	}
	
	@GetMapping("/work-area/{workAreaId}")
	public WorkAreaDto getWorkArea(@PathVariable int workAreaId) {
		
		WorkArea theWorkArea = workAreaService.getWorkArea(workAreaId);
		
		if (theWorkArea == null) {
			throw new EntityNotFoundException("Id " +workAreaId+" no encontrado");
		}
		
		return convertToDto(theWorkArea);
	}
	
	private WorkAreaDto convertToDto(WorkArea model) {
		WorkAreaDto dto = modelMapper.map(model, WorkAreaDto.class);
		dto.setCountryId(model.getCountry().getId());
		dto.setCountryCode(model.getCountry().getCode());
	    return dto;
	}
}
