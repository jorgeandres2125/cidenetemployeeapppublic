package co.com.cidenet.rest.Country;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.cidenet.domain.entity.Country;
import co.com.cidenet.presentation.error.EntityNotFoundException;
import co.com.cidenet.presentation.dto.CountryDto;
import co.com.cidenet.service.CountryService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT, RequestMethod.DELETE})
public class CountryRestController {

	@Autowired
	private CountryService countryService;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@GetMapping("/countries")
	public List<CountryDto> getCountries() {
		
		List<Country> list = countryService.getCountries(); 
		if(list == null || list.isEmpty()) {
			throw new EntityNotFoundException("No se encontraron paises");
		}
		return list.stream()
		          .map(this::convertToDto)
		          .collect(Collectors.toList());
	}
	
	@GetMapping("/country/{countryId}")
	public CountryDto getCountry(@PathVariable int countryId) {
		
		Country theCountry = countryService.getCountry(countryId);
		
		if (theCountry == null) {
			throw new EntityNotFoundException("Id " + countryId+" no encontrado");
		}
		
		return convertToDto(theCountry);
	}
	
	private CountryDto convertToDto(Country model) {
		CountryDto dto = modelMapper.map(model, CountryDto.class);
	    return dto;
	}
	
}
