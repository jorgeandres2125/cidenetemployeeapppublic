package co.com.cidenet.service;

import java.util.List;

import co.com.cidenet.domain.entity.Country;

/**
* <h1>CountryService</h1>
* Acceso a datos para la tabla Country.
*
*/
public interface CountryService {
  /**
   * Consulta la lista de paises de la tabla Country.
   * @return List<Country> Lista de Paises.
   */
	public List<Country> getCountries();

	
   /**
   * Busca y retorna un pais por Id.
   * @param theId Id del pais que se consulta
   * @return Country Pais consultado en la tabla Country.
   */	
	public Country getCountry(int theId);
}
