package co.com.cidenet.service;

import java.util.List;

import co.com.cidenet.domain.entity.IdentificationType;
import co.com.cidenet.domain.entity.WorkArea;

/**
* <h1> WorkAreaService</h1>
* Acceso a datos para la tabla WorkArea.
*
*/
public interface WorkAreaService {
  /**
   * Consulta la lista de areas de trabajo por pais.
   * @param countryCode  Identificacion del pais que retornaran la lista de areas de trabajo
   * @return List<WorkArea> Lista de Areas de trabajo.
   */
	public List<WorkArea> getWorkAreasByCountry(String countryCode);
	
  /**
   * Busca y retorna un Area de trabajo por Id.
   * @param theId Id del area de trabajo  que se consulta
   * @return WorkArea area de trabajo consultado en la tabla WorkArea.
   */	
	public WorkArea getWorkArea(int theId);
}
