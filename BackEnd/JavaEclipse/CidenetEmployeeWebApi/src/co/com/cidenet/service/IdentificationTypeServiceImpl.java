package co.com.cidenet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.cidenet.domain.IdentificationType.IdentificationTypeDAO;
import co.com.cidenet.domain.entity.IdentificationType;

@Service
public class IdentificationTypeServiceImpl implements IdentificationTypeService{
	
	@Autowired
	private IdentificationTypeDAO identificationTypeDAO;
	
	@Override
	@Transactional	
	public List<IdentificationType> getIdentificationTypesByCountry(String countryCode) {
		return identificationTypeDAO.getIdentificationTypesByCountry(countryCode);
	}

	@Override
	@Transactional
	public IdentificationType getIdentificationType(int theId) {
		return identificationTypeDAO.getIdentificationType(theId);
	}

}
