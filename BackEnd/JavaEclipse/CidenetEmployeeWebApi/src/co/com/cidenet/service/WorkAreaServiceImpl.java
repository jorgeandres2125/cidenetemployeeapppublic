package co.com.cidenet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.cidenet.domain.WorkArea.WorkAreaDAO;
import co.com.cidenet.domain.entity.WorkArea;


@Service
public class WorkAreaServiceImpl implements WorkAreaService {

	
	@Autowired
	private WorkAreaDAO workAreaDAO;
	
	
	@Override
	@Transactional	
	public List<WorkArea> getWorkAreasByCountry(String countryCode) {
		return workAreaDAO.getWorkAreasByCountry(countryCode);
	}

	@Override
	@Transactional	
	public WorkArea getWorkArea(int theId) {
		return workAreaDAO.getWorkArea(theId);
	}

}
