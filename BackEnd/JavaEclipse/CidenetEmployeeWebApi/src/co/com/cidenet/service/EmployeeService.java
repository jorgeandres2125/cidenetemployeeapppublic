package co.com.cidenet.service;

import java.util.List;

import org.springframework.stereotype.Service;

import co.com.cidenet.domain.entity.Employee;



public interface EmployeeService {
	
	public List<Employee> getEmployees();
}
