package co.com.cidenet.domain.IdentificationType;

import java.util.List;

import co.com.cidenet.domain.entity.IdentificationType;

/**
* <h1>IdentificationTypeDAO</h1>
* Acceso a datos para la tabla IdentificationType.
*
*/
public interface IdentificationTypeDAO {

  /**
   * Consulta la lista de tipo de identificaciones por pais.
   * @param countryCode  Identificacion del pais que retornaran la lista de tipos de identificaciones
   * @return List<IdentificationType> Lista de tipos de identificaciones.
   */
	public List<IdentificationType> getIdentificationTypesByCountry(String countryCode);
	
  /**
   * Busca y retorna un Tipo de Identificacion por Id.
   * @param theId Id del tipo de identificación  que se consulta
   * @return IdentificationType Tipo de identificación consultado en la tabla IdentificationType.
   */	
	public IdentificationType getIdentificationType(int theId);
}
