package co.com.cidenet.domain.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Identification")
public class Identification {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	@Column(name="Value")
	private String value;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
		 	CascadeType.DETACH, CascadeType.REFRESH})
		@JoinColumn(name="IdentificationTypeId")
	private IdentificationType identificationType;
	
	
	@OneToOne(mappedBy="identification", 
			cascade={CascadeType.ALL})
	private Employee employee;

	
	public Identification() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	


	public Identification(int id, String value, IdentificationType identificationType, Employee employee) {
		super();
		this.id = id;
		this.value = value;
		this.identificationType = identificationType;
		this.employee = employee;
	}





	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public IdentificationType getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(IdentificationType identificationType) {
		this.identificationType = identificationType;
	}





	public Employee getEmployee() {
		return employee;
	}





	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
}
