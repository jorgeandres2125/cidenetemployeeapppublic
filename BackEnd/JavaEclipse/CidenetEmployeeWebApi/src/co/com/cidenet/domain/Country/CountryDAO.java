package co.com.cidenet.domain.Country;

import java.util.List;

import co.com.cidenet.domain.entity.Country;



/**
* <h1>CountryDAO</h1>
* Acceso a datos para la tabla Country.
*
*/
public interface CountryDAO {
   /**
   * Consulta la lista de paises de la tabla Country.
   * @return List<Country> Lista de Paises.
   */
	public List<Country> getCountries();

	
   /**
   * Busca y retorna un pais por Id.
   * @param theId Id del pais que se consulta
   * @return Country Pais consultado en la tabla Country.
   */	
	public Country getCountry(int theId);
}
