package co.com.cidenet.domain.IdentificationType;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.com.cidenet.domain.entity.IdentificationType;

@Repository
public class IdentificationTypeDAOImpl implements IdentificationTypeDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<IdentificationType> getIdentificationTypesByCountry(String countryCode) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query<IdentificationType> theQuery = currentSession
				.createQuery("from IdentificationType where country.code = :countryCode order by name",IdentificationType.class)
				.setParameter("countryCode", countryCode);
		List<IdentificationType> identificationTypes = theQuery.getResultList();
		return identificationTypes;
	}

	@Override
	public IdentificationType getIdentificationType(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();
		IdentificationType theIdentificationType = currentSession.get(IdentificationType.class, theId);
		return theIdentificationType;
	}

}
