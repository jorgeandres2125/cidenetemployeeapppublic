package co.com.cidenet.domain.Country;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.com.cidenet.domain.entity.Country;

@Repository
public class CountryDAOImpl implements CountryDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Country> getCountries() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query<Country> theQuery = currentSession.createQuery("from Country order by name",Country.class);
		List<Country> countries = theQuery.getResultList();
		return countries;
	}

	@Override
	public Country getCountry(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Country theCountry = currentSession.get(Country.class, theId);
		return theCountry;
	}

}
