package co.com.cidenet.domain.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Employee")
public class Employee {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	@Column(name="FirstName")
	private String firstName;
	
	@Column(name="MiddleName")
	private String middleName;
	
	@Column(name="LastName")
	private String lastName;
	
	@Column(name="SecondSurename")
	private String secondSurename;
	
	@Column(name="Email")
	private String email;
	
	@Column(name = "DateOfJoining")
	@Temporal(TemporalType.DATE)
	private Date dateOfJoining;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
		 	CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name="CountryId")
	private Country country;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
		 	CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name="WorkAreaId")
	private WorkArea workArea;
	
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="IdentificationId")
	private Identification identification;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSecondSurename() {
		return secondSurename;
	}

	public void setSecondSurename(String secondSurename) {
		this.secondSurename = secondSurename;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public WorkArea getWorkArea() {
		return workArea;
	}

	public void setWorkArea(WorkArea workArea) {
		this.workArea = workArea;
	}

	public Identification getIdentification() {
		return identification;
	}

	public void setIdentification(Identification identification) {
		this.identification = identification;
	}
	
	
	
	
}
