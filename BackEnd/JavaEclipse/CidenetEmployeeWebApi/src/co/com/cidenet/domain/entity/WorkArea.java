package co.com.cidenet.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="WorkArea")
public class WorkArea {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	@Column(name="Name")
	private String name;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
			 	CascadeType.DETACH, CascadeType.REFRESH})
	@JoinColumn(name="CountryId")
	private Country country;
	
	@OneToMany(fetch=FetchType.LAZY,
			   mappedBy="workArea",
			   cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						 CascadeType.DETACH, CascadeType.REFRESH})
	private List<Employee> employees;
	
	
	public WorkArea() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public WorkArea(int id, String name, Country country) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
    public void add(Employee tempEmployee) {
		
		if (employees == null) {
			employees = new ArrayList<Employee>();
		}
		
		employees.add(tempEmployee);
		tempEmployee.setWorkArea(this);
	}
	
	
}
