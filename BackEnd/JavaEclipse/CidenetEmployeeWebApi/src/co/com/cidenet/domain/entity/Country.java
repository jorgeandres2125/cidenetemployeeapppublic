package co.com.cidenet.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Country")
public class Country {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	@Column(name="Code")
	private String code;
	
	@Column(name="Name")
	private String name;
	
	@Column(name="EmailDomain")
	private String emailDomain;
	
	@OneToMany(fetch=FetchType.LAZY,
			   mappedBy="country",
			   cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						 CascadeType.DETACH, CascadeType.REFRESH})
	private List<IdentificationType> identificationTypes;
	
	@OneToMany(fetch=FetchType.LAZY,
			   mappedBy="country",
			   cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						 CascadeType.DETACH, CascadeType.REFRESH})
	private List<WorkArea> workAreas;
	
	@OneToMany(fetch=FetchType.LAZY,
			   mappedBy="country",
			   cascade= {CascadeType.PERSIST, CascadeType.MERGE,
						 CascadeType.DETACH, CascadeType.REFRESH})
	private List<Employee> employees;
	
	public Country() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public Country(int id, String code, String name, String emailDomain) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.emailDomain = emailDomain;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}
	
	
	public List<IdentificationType> getIdentificationTypes() {
		return identificationTypes;
	}


	public void setIdentificationTypes(List<IdentificationType> identificationTypes) {
		this.identificationTypes = identificationTypes;
	}
	
	

	public List<WorkArea> getWorkAreas() {
		return workAreas;
	}



	public void setWorkAreas(List<WorkArea> workAreas) {
		this.workAreas = workAreas;
	}



	
	public List<Employee> getEmployees() {
		return employees;
	}


	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}



	public void add(IdentificationType tempIdentificationType) {
		
		if (identificationTypes == null) {
			identificationTypes = new ArrayList<IdentificationType>();
		}
		
		identificationTypes.add(tempIdentificationType);
		tempIdentificationType.setCountry(this);
	}
	
	public void add(WorkArea tempWorkArea) {
		
		if (workAreas == null) {
			workAreas = new ArrayList<WorkArea>();
		}
		
		workAreas.add(tempWorkArea);
		tempWorkArea.setCountry(this);
	}
	
	public void add(Employee tempEmployee) {
		
		if (employees == null) {
			employees = new ArrayList<Employee>();
		}
		
		employees.add(tempEmployee);
		tempEmployee.setCountry(this);
	}
}
