package co.com.cidenet.domain.Employee;

import java.util.List;

import co.com.cidenet.domain.entity.Employee;

public interface EmployeeDAO {
	/**
   * Consulta la lista de Empleasdos en la tabla Employee.
   * @return List<Employee> Lista de Empleados.
   */
	public List<Employee> getEmployees();
}
